//
//  BluetoothLocalCentralManagePeripheral.swift
//  Fairy
//
//  Created by iOS on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral : PeripheralProxyDelegate {
    
    func peripheral(peripheral: BluetoothRemotePeripheral, didModifyServices invalidatedServices: [BluetoothRemoteService]) {
        
    }
    
    func peripheralDidDiscoverCharacteristics(peripheral: BluetoothRemotePeripheral, forService service: BluetoothRemoteService, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		verbosity.DEBUG("Discovered characteristics : \(service.characteristics?.debugDescription) on peripheral : \(peripheral.debugDescription)")
		let bError : BluetoothError = (error != nil) ? .ErrorDiscoveringCharacteristics : .NoError
		delegate?.didDiscoverCharacteristics?(service.characteristics, error: bError)
		bluetoothDiscoveredCharacteristics?(characteristics: service.characteristics, error: bError)
    }
	
    func peripheralDidDiscoverDescriptors(peripheral: BluetoothRemotePeripheral, forCharacteristic characteristic: BluetoothRemoteCharacteristic, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
        
    }
    
    func peripheralDidDiscoverIncludedServices(peripheral: BluetoothRemotePeripheral, forService service: BluetoothRemoteService, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
        
    }
    
    func peripheralDidDiscoverServices(peripheral: BluetoothRemotePeripheral, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		verbosity.DEBUG("Discovered services \(peripheral.services?.debugDescription) on peripheral \(peripheral.debugDescription)")
		let bError : BluetoothError = (error != nil) ? .ErrorDiscoveringServices : .NoError
		
		delegate?.didDiscoverServices?(peripheral.services, error: bError)
		bluetoothDiscoveredServices?(services: peripheral.services, error:bError)
    }
	
    func peripheralDidInvalidateServices(peripheral: BluetoothRemotePeripheral) {
        
    }
    
    func peripheralDidUpdateName(peripheral: BluetoothRemotePeripheral) {
        
    }
    
    func peripheralDidUpdateNotificationStateForCharacteristic(peripheral: BluetoothRemotePeripheral, characteristic: BluetoothRemoteCharacteristic, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		
		verbosity.DEBUG("Did update notification state for characteristic \(characteristic) on peripheral \(peripheral)")
		if error != nil {
			verbosity.ERROR("State update failed with error \(error)")
		}
		if characteristic.UUIDString == BluetoothLEProtocolDefinedService.Battery.characteristics[0] {
			peripheral.readValueForCharacteristic(characteristic)
		}
    }
	
    func peripheralDidUpdateRSSI(peripheral: BluetoothRemotePeripheral, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		verbosity.DEBUG("Updated on RSSI of peripheral:\(peripheral)")
		if let err = error {
			verbosity.ERROR("\(err)")
		}
		else {
			delegate?.deviceDidUpdateRSSI?(peripheral, RSSI: peripheral.RSSI)
		}
    }
	
    func peripheralDidUpdateValueForCharacteristic(peripheral: BluetoothRemotePeripheral, characteristic: BluetoothRemoteCharacteristic, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		verbosity.INFO("Received update for characteristic \(characteristic.UUIDString) with value : \(characteristic.value?.description)")
		
		let bError : BluetoothError = (error != nil) ? .ErrorReadingCharacteristicValue : .NoError
		var isBatteryCharacteristic : Bool = false
		
		isBatteryCharacteristic = characteristic.UUIDString == BluetoothLEProtocolDefinedService.Battery.characteristics[0]
		if isBatteryCharacteristic {
			var batteryPercentage : UInt8 = 0
			if let percentageData = characteristic.value {
				var level : UInt8 = 0
				percentageData.getBytes(&level, length: 1)
				if level >= 0 {
					batteryPercentage = level
				}
			}
			peripheral.batterylevel = NSNumber(long: Int(batteryPercentage))
			delegate?.deviceDidUpdateBatteryLevel?(peripheral, batteryLevel: NSNumber(long: Int(batteryPercentage)))
			
		}
		if !isBatteryCharacteristic {
			delegate?.didReadCharacteristicValue?(characteristic.value, error: bError)
			bluetoothReadCharacteristic?(data: characteristic.value, error: bError)
			
			if bluetoothReadCharacteristic == nil {
				delegate?.didReceiveValueChangedNotificationWithCharacteristicValue?(characteristic.value, error: bError)
				bluetoothNotifiedCharacteristic?(data: characteristic.value, error: bError)
			}
			
			bluetoothReadCharacteristic = nil
			
//			var isNewValue : Bool = false
//			var exists : Bool = false
//			for ch in subscribedCharacteristics {
//				if ch.UUIDString == characteristic.UUIDString {
//					if let newValue = characteristic.value {
//						if let equal = ch.value?.isEqualToData(newValue) {
//							isNewValue = !equal
//						}
//					}
//					exists = true
//					break
//				}
//			}
//			
//			if !exists {
//				subscribedCharacteristics.append(characteristic)
//				isNewValue = true
//			}
//			
//			if isNewValue {
//				delegate?.didReceiveValueChangedNotificationWithCharacteristicValue?(characteristic.value, error: bError)
//				bluetoothNotifiedCharacteristic?(data: characteristic.value, error: bError)
//			}
		}
    }
	
    func peripheralDidUpdateValueForDescriptor(peripheral: BluetoothRemotePeripheral, descriptor: BluetoothRemoteDescriptor, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		
    }
	
    func peripheralDidWriteValueForCharacteristic(peripheral: BluetoothRemotePeripheral, characteristic: BluetoothRemoteCharacteristic, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		
		verbosity.DEBUG("Did write value to characteristic \(characteristic.debugDescription) of peripheral:\(peripheral.debugDescription)")
		let bError : BluetoothError = (error != nil) ? .ErrorOnWritingValueToCharacteristic : .NoError
		if bError != .NoError { verbosity.ERROR("\(error)") }
		
		delegate?.didWriteValueToCharacteristic?(characteristic.value, error: bError)
		bluetoothWriteToCharacteristic?(data: characteristic.value, error: bError)
    }
	
    func peripheralDidWriteValueForDescriptor(peripheral: BluetoothRemotePeripheral, descriptor: BluetoothRemoteDescriptor, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
        
    }
}


