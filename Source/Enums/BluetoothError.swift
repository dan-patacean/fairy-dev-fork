//
//  BluetoothError.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc public enum BluetoothError : Int {
	case NoError
	case ScanningIsNotAvailable
	case DeviceWithProvidedInfoNotFound
	case ConnectionTimedOut
	case FailedToConnectToDevice
	case DeviceProvidedIsNotConnected
	case ErrorDiscoveringServices
	case ErrorDiscoveringCharacteristics
	case ServiceNotProvidedByDevice
	case CharacteristicNotProvidedByDevice
	case ErrorReadingCharacteristicValue
	case ErrorWritingNilValueToCharacteristic
	case ErrorOnWritingValueToCharacteristic
	case ErrorAddingService
	case ErrorOnStartingAdvertisement
	case DeviceDisconnected
    case ErrorUpdatingRSSIValue
    case ErrorDiscoveringIncludedServices
    case ErrorDiscoveringCharacteristicDescriptors
    case ErrorUpdatingCharacteristicDescriptors
    case ErrorWritingCharacteristicDescriptors
    case ErrorUpdatingCharacteristicValue
    case ErrorUpdatingCharacteristicNotificationState
    
}

extension BluetoothError : StringConvertible {
	public func string() -> String {
		return [
		"No Error",
		"Scanning Is Not Available",
		"Device With Provided Info Not Found",
		"Connection Timed Out",
		"Failed To Connect To Device",
		"Device Provided Is Not Connected",
		"Error Discovering Services",
		"Error Discovering Characteristics",
		"Service Not Provided By Device",
		"Characteristic Not Provided By Device",
		"Error Reading Characteristic Value",
		"Error Writing Nil Value To Characteristic",
		"Error On Writing Value To Characteristic",
		"Error Adding Service",
		"Error On Starting Advertising",
		"Device Disconnected",
        "Error Updating RSSI Value",
        "Error Discovering Included Services",
        "Error Discovering Characteristic Descriptors",
        "Error Updating Characteristic Descriptors",
        "Error Writing Characteristic Descriptors",
        "Error Updating Characteristic Value",
        "Error Updating Characteristic Notification State"
		][self.rawValue]
	}
}