//
//  BluetoothRemoteService.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public class BluetoothRemoteService: NSObject, BluetoothUUID {
    private var cbservice: CBService
    
    var UUIDString: String {
        get {
            return stringFrom(cbservice.UUID)
        }
    }
    
    var peripheral: BluetoothRemotePeripheral {
        return BluetoothRemotePeripheral(peripheral: cbservice.peripheral)
    }
    
    var isPrimary: Bool {
        return cbservice.isPrimary
    }
    
    var includedServices: [BluetoothRemoteService]? {
        return cbservice.includedServices?.flatMap { BluetoothRemoteService (service: $0) }
    }
    
    var characteristics: [BluetoothRemoteCharacteristic]? {
        return cbservice.characteristics?.flatMap { BluetoothRemoteCharacteristic(characteristic: $0) }
    }
    
    var CoreBluetoothService: CBService {
        get {
            return cbservice
        }
    }
    
    init(service: CBService) {
        cbservice = service
    }
    
    public override var description: String {
        return cbservice.description
    }
    
    public override var debugDescription: String {
        return cbservice.debugDescription
    }
}