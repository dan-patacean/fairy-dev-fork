//
//  BluetoothLocalPeripheralAdvertising.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalPeripheral {

	public func startAdvertising(withErrorBlock withErrorBlock : FailedToStartAdvertisingBlock) {
		guard peripheralManagerIsReady else { verbosity.INFO("Peripheral is not ready to advertise"); return }
		guard !isAdvertising else { verbosity.INFO("Peripheral is already advertising\n"); return }
		
		verbosity.DEBUG("Before advertising services were \(peripheralConfiguration?.peripheralServices().debugDescription)\n")
		
		if let uuids : [CBUUID] = peripheralConfiguration?.peripheralServices().flatMap({ $0.UUID }) {
			verbosity.DEBUG("Advertisement started with UUIDs  \(uuids.debugDescription)\n")
			manager?.startAdvertising([
				CBAdvertisementDataServiceUUIDsKey : uuids,
				CBAdvertisementDataLocalNameKey : peripheralConfiguration?.peripheralName ?? ""
				])
		} else {
			verbosity.DEBUG("Advertisement started without keys or uuids\n")
			manager?.startAdvertising(nil)
		}
		
		bluetoothFailedToStartAdvertising = withErrorBlock
		isAdvertising = true
	}
	
	public func stopAdvertising() {
		verbosity.DEBUG("Advertisement stoped\n")
		manager?.stopAdvertising()
		isAdvertising = false
	}
	
}
