//
//  BluetoothLocalCentralDiscoverServices.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func discoverServicesOfDevice(identifier identifier : NSUUID, limitSearchToServiceUUIDs: [String]? = nil, discoveryServicesBlock : DiscoveredServicesBlock = nil){
		if let connectedPeripheral = getConnectedPeripheralByIdentifier(identifier) {
			discoverServices(serviceUUIDs: limitSearchToServiceUUIDs, peripheral: connectedPeripheral, servicesDiscoveredBlock: discoveryServicesBlock)
		} else {
			discoveryServicesBlock?(services: nil, error: .DeviceProvidedIsNotConnected)
		}
	}
	
	
	internal func discoverServices(serviceUUIDs serviceUUIDs : [String]?, peripheral : BluetoothRemotePeripheral, servicesDiscoveredBlock : DiscoveredServicesBlock ) {
		bluetoothDiscoveredServices = servicesDiscoveredBlock
		// Add cache
		peripheral.delegate = peripheralDelegateProxy
		var services : [CBUUID]?
		if let s = serviceUUIDs {
			services = s.flatMap{ CBUUID(string: $0 as String) }
		}
		else {
			services = nil
		}
		peripheral.CoreBluetoothPeripheral.discoverServices(services)
	}
	
}