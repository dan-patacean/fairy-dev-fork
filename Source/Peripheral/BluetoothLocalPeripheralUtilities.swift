//
//  BluetoothLocalPeripheralUtilities.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public extension BluetoothLocalPeripheral {
	
	public func updateValueOfCharacteristic(
		 characteristicUUID : String,
		forService serviceUUID: String,
		withValue value : NSData?
		) -> Bool {
			guard let success = peripheralConfiguration?.updateCharacteristic(CBUUID(string: characteristicUUID), forService: CBUUID(string: serviceUUID), withNewValue: value) else {
				verbosity.DEBUG("Peripheral has no configuration")
				return false
			}
			// Notify process
			if success {
				if let value = value {
					if let found = peripheralConfiguration?.peripheralHas(characteristic: CBUUID(string: characteristicUUID), forService: CBUUID(string: serviceUUID)) {
						if let localCharacteristic = found.characteristic {
							manager?.updateValue(value, forCharacteristic: localCharacteristic.CoreBluetoothCharacteristic, onSubscribedCentrals: nil)
						}
					}
				}
			}
			return success
	}
}