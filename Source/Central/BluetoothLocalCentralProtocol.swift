//
//  BluetoothLocalCentralProtocol.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation

@objc public protocol BluetoothLocalCentralDelegate {
	
	optional func didChangedBluetoothState(state : BluetoothState)
	
    //MARK: Scanning
    optional func didFailToScanPeripherals(error : BluetoothError)
    optional func didDiscoverPeripherals(peripherals : [BluetoothRemotePeripheral])
    
    //MARK: Connection
    optional func didFailToConnectPeripheral(peripheral: BluetoothRemotePeripheral?, error: BluetoothError)
    optional func didConnectPeripheral(peripheral: BluetoothRemotePeripheral)
    optional func didDisconnectFromPeripheral(peripheral: BluetoothRemotePeripheral, error: BluetoothError)
    
	//MARK: Discovering
	optional func didDiscoverServices(services : [BluetoothRemoteService]?, error : BluetoothError)
	optional func didDiscoverCharacteristics(characteristics : [BluetoothRemoteCharacteristic]?, error : BluetoothError)
	
    //MARK: Battery
    optional func deviceDidUpdateBatteryLevel(peripheral: BluetoothRemotePeripheral, batteryLevel : NSNumber)
	optional func deviceDidUpdateRSSI(peripheral: BluetoothRemotePeripheral, RSSI : NSNumber)
	
	//MARK: Communication
	optional func didWriteValueToCharacteristic(data : NSData?, error : BluetoothError)
	
    //MARK: Deprecated

//  optional func failedToConnectToDevice(error : BluetoothError)
//	optional func connectedToDevice(name : NSString, identifier : NSUUID)
//	optional func disconnectedFromDevice(name : NSString, identifier : NSUUID)
//	optional func deviceDidUpdateBatteryLevel(name : NSString, identifier : NSUUID, batteryLevel : NSNumber)
//	optional func deviceDidUpdateRSSI(name : NSString, identifier : NSUUID, RSSI : NSNumber)
//	optional func didDiscoverServices(services : NSArray?, error : BluetoothError)
//	optional func didDiscoverCharacteristics(characteristics : NSArray?, error : BluetoothError)
	
	
	optional func didReadCharacteristicValue(data : NSData?, error : BluetoothError)
	
	optional func didReceiveValueChangedNotificationWithCharacteristicValue(data : NSData?, error : BluetoothError)
	
}