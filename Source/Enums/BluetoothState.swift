//
//  BluetoothState.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc public enum BluetoothState : Int {
	case Unknown
	case Unsupported
	case Unauthorized
	case Resetting
	case PoweredOff
	case PoweredOn
}

internal extension BluetoothState {
	
	internal init(state : CBCentralManagerState) {
		switch state {
		case .Unknown : self = .Unknown
		case .Unsupported : self = .Unsupported
		case .Unauthorized : self = .Unauthorized
		case .Resetting : self = .Resetting
		case .PoweredOff : self = .PoweredOff
		case .PoweredOn : self = .PoweredOn
		}
	}
	
	internal init(state : CBPeripheralManagerState) {
		switch state {
		case .Unknown : self = .Unknown
		case .Unsupported : self = .Unsupported
		case .Unauthorized : self = .Unauthorized
		case .Resetting : self = .Resetting
		case .PoweredOff : self = .PoweredOff
		case .PoweredOn : self = .PoweredOn
		}
	}
	
}

extension BluetoothState: StringConvertible {
	public func string() -> String {
		return ["Unknown", "Unsupported", "Unauthorized", "Resetting", "Powered Off", "Powered On"][self.rawValue]
	}
}