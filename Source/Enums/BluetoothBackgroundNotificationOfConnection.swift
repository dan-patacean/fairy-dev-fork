//
//  BluetoothBackgroundNotificationOfConnection.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc public enum BackgroundNotificationOfConnection : Int {
	case None
	case OnDisconnection
	#if os(iOS)
	case OnConnection
	case OnNotification
	case OnConnectionAndDisconnection
	case OnConnectionAndNotification
	case OnDisconnectionAndNotification
	case All
	#endif
}
