//
//  BluetoothPeripheral.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public protocol BluetoothPeripheralDelegate {
	
}

public class BluetoothRemotePeripheral : NSObject {
	
	private var cbperipheral : CBPeripheral
	private var advertisedata : [String : AnyObject]?
	internal var batterylevel : NSNumber?
	internal var rssi : NSNumber?
	
	public var advertismentData : [String : AnyObject]? {
		return advertisedata
	}
	
	public var identifier : NSUUID {
		return cbperipheral.identifier
	}
	
	public var UUIDString : String {
		return identifier.UUIDString
	}
	
	public var name : String? {
        return cbperipheral.name?.characters.count > 0 ? cbperipheral.name : "Unnamed Peripheral"
	}
	
	public var linkstate : BluetoothPeripheralLinkState {
		return BluetoothPeripheralLinkState(state: cbperipheral.state)
	}
	
	public var RSSI : NSNumber {
		return rssi ?? -1 * Double.infinity
	}
	
	public var BatteryLevel : NSNumber {
		return batterylevel ?? -1 * Double.infinity
	}
	
    var services : [BluetoothRemoteService]? {
		return cbperipheral.services?.flatMap{ BluetoothRemoteService(service: $0) }
	}
	
	public var delegate : CBPeripheralDelegate? {
		get {
			return cbperipheral.delegate
		}
		set {
			cbperipheral.delegate = newValue
		}
	}
		
	public var CoreBluetoothPeripheral : CBPeripheral {
		get {
			return cbperipheral
		}
	}
	
	internal init(peripheral : CBPeripheral, advertisementData : [String : AnyObject], RSSI : NSNumber) {
		self.cbperipheral = peripheral
		self.advertisedata = advertisementData
		self.rssi = RSSI
	}
	
	internal init(peripheral : CBPeripheral) {
		self.cbperipheral = peripheral
	}
    
    func readRSSI() {
        cbperipheral.readRSSI()
    }
	
	func readValueForCharacteristic(c : BluetoothRemoteCharacteristic) {
		cbperipheral	.readValueForCharacteristic(c.CoreBluetoothCharacteristic)
	}
}