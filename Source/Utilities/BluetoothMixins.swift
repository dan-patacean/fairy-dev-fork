//
//  JMBluetoothMixins.swift
//  JMBluetooth
//
//  Created by iOS on 13/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

internal protocol BluetoothUUID {
    var UUIDString : String { get }
}

extension BluetoothUUID {
    
    var UUID : CBUUID {
        return CBUUID(string: UUIDString)
    }
	
	func stringFrom(cbuuid : CBUUID)  -> String{
		#if os(iOS)
			return cbuuid.UUIDString
		#elseif os(OSX)
			if #available(OSX 10.10, *) {
				return cbuuid.UUIDString
			}
			else {
				return NSUUID(UUIDBytes: UnsafePointer<UInt8>(cbuuid.data.bytes)).UUIDString
			}
		#endif
	}
}

public protocol StringConvertible: CustomStringConvertible, CustomDebugStringConvertible {
	func string() -> String
}

public extension StringConvertible {
	
	func string() -> String {
		return "\(self)"
	}
	
	var description:String {
		get {
			return string()
		}
	}
	
	var debugDescription:String {
		get {
			return string()
		}
	}
}
